import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CoursesComponent} from './Courses.component';
import { CourseComponent } from './course/course.component';
import {CoursesService} from "./Courses.service";
import { CardComponent } from './card/card.component';
import { HeaderComponent } from './header/header.component';
import { OverviewComponent } from './overview/overview.component';
import { PreHeadphonesOverviewComponent } from './pre-headphones-overview/pre-headphones-overview.component';
import { SingleHeadphoneComponent } from './pre-headphones-overview/single-headphone/single-headphone.component';
import { NewheadphonesOverviewComponent } from './newheadphones-overview/newheadphones-overview.component';
import { BuyheadphonesComponent } from './buyheadphones/buyheadphones.component';
import { WhyheadphonesComponent } from './whyheadphones/whyheadphones.component';
import { MapComponent } from './map/map.component';
import { FooterComponent } from './footer/footer.component';
import { NavigationComponent } from './navigation/navigation.component';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseComponent,
    CardComponent,
    HeaderComponent,
    OverviewComponent,
    PreHeadphonesOverviewComponent,
    SingleHeadphoneComponent,
    NewheadphonesOverviewComponent,
    BuyheadphonesComponent,
    WhyheadphonesComponent,
    MapComponent,
    FooterComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    CoursesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
