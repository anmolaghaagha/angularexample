import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewheadphonesOverviewComponent } from './newheadphones-overview.component';

describe('NewheadphonesOverviewComponent', () => {
  let component: NewheadphonesOverviewComponent;
  let fixture: ComponentFixture<NewheadphonesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewheadphonesOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewheadphonesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
