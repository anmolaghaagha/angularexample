import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreHeadphonesOverviewComponent } from './pre-headphones-overview.component';

describe('PreHeadphonesOverviewComponent', () => {
  let component: PreHeadphonesOverviewComponent;
  let fixture: ComponentFixture<PreHeadphonesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreHeadphonesOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreHeadphonesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
